//
//  task_1.h
//  ShilovNJrProject
//
//  Created by Шилов Николай Николаевич on 30.04.2024.
//

#ifndef task_1_h
#define task_1_h


#endif /* task_1_h */

#include <iostream>
#include <typeinfo>

// Container class:
template <typename Type>
class Container {
    
private:
    
    // Container's element struct:
    struct ContainerElement {
        
        Type data;
        ContainerElement * prev;
        ContainerElement * next;
        
        // Initialization:
        ContainerElement(Type const & data) {
            
            this -> data = data;
            this -> prev = nullptr;
            this -> next = nullptr;
            
        }
        
    };
    
    ContainerElement * first;
    ContainerElement * last;
    uint64_t size;
    
public:
    
    // Initialization:
    Container() {
        
        this -> first = nullptr;
        this -> last = nullptr;
        this -> size = 0;
        
    }
    
    // Destructor:
    ~Container() {
        
        clear();
        
    }
    
    // Clear the container:
    void clear() {
        
        // Iterate over the container, deleting its items:
        while (first) {
            
            // Temporary pointer to the current first element:
            ContainerElement * first_temp = first;
            
            // Shift the first element in the container:
            first = first -> next;
            
            // Delete the temporary element:
            delete first_temp;
            
        }
        
        // Set the last item to the nullptr and zero out the container's size:
        last = nullptr;
        size = 0;
        
    }
    
    // Check if the container is empty:
    bool is_empty() const {
        
        return (size == 0);
        
    }
    
    // Get the first item' data:
    Type & get_first() const {
        
        // Empty container case:
        if (is_empty()) {
            
            throw std::out_of_range("Container is empty");
            
        }
        
        return first -> data;
        
    }
    
    // Get the last item's data:
    Type & get_last() const {
        
        // Empty container case:
        if (is_empty()) {
            
            throw std::out_of_range("Container is empty");
            
        }
        
        return last -> data;
        
    }
    
    // Get size of the container:
    uint64_t get_size() const {
        
        return size;
        
    }
    
    // Prepend an element to the container's start:
    void prepend(Type const & data) {
        
        // Create a new item:
        ContainerElement * new_item = new ContainerElement(data);
        
        // Empty container case:
        if (is_empty()) {
            
            first = last = new_item;
            
        }
        
        // Non-empty container case:
        else {
            
            first -> prev = new_item;
            new_item -> next = first;
            first = new_item;
            
        }
        
        // Increase the container's size:
        ++size;
        
    }
    
    // Append an element to the container's end:
    void append(Type const & data) {
        
        // Create a new item:
        ContainerElement * new_item = new ContainerElement(data);
        
        // Empty container case:
        if (is_empty()) {
            
            first = last = new_item;
            
        }
        
        // Non-empty container case:
        else {
            
            last -> next = new_item;
            new_item -> prev = last;
            last = new_item;
            
        }
        
        // Increase the container's size:
        size++;
        
    }
    
    // Pop the first item:
    void pop_first() {
        
        // Empty container case:
        if (is_empty()) {
            
            throw std::out_of_range("Container is empty");
            
        }
        
        // Non-empty container case:
        else {
            
            // Temporary pointer to the current first element:
            ContainerElement * first_temp = first;
            
            // Shift the first element in the container:
            first = first -> next;
            
            // Check if the container still contains elements:
            if (first) {
                
                first -> prev = nullptr;
                
            }
            
            // If it does not, change the last element too:
            else {
                
                last = nullptr;
                
            }
            
            // Delete the temporary element and decrease the container's size:
            delete first_temp;
            size--;
            
        }
        
    }
    
    // Pop the last item:
    void pop_last() {
        
        // Empty container case:
        if (is_empty()) {
            
            throw std::out_of_range("Container is empty");
            
        }
        
        // Non-empty container case:
        else {
            
            // Temporary pointer to the current first element:
            ContainerElement * last_temp = last;
            
            // Shift the last element in the container:
            last = last -> prev;
            
            // Check if the container still contains elements:
            if (last) {
                
                last -> next = nullptr;
                
            }
            
            // If it does not, change the first element too:
            else {
                
                first = nullptr;
                
            }
            
            // Delete the temporary element and decrease the container's size:
            delete last_temp;
            size--;
            
        }
        
    }
    
    // Swap the contents with another container:
    void swap(Container & other) {
        
        std::swap(first, other.first);
        std::swap(last, other.last);
        std::swap(size, other.size);
    
    }
    
    // Reverse the elements' order:
    void reverse() {
        
        
        // Temporary pointers:
        ContainerElement * current = first;
        ContainerElement * temp = nullptr;
        
        // Iterate over the container, reversing the links between its elements:
        while (current != nullptr) {
            
            temp = current -> prev;
            current -> prev = current -> next;
            current -> next = temp;
            current = current -> prev;
            
        }
        
        // Reset the first and the last elements in nonzero / non single-element container case:
        if (temp != nullptr) {
            
            last = first;
            first = temp -> prev;
            
        }
        
    }
    
    // Container's iterator class:
    class Iterator {
        
    private:
        
        ContainerElement * current;
        
    public:
        
        // Initialization:
        Iterator(ContainerElement * item) {
            
            this -> current = item;
            
        }
        
        // Dereferencing operator:
        Type & operator*() const {
            
            return current -> data;
            
        }
        
        // Incrememntation operator:
        Iterator & operator++() {
            
            if (current) {
                
                current = current -> next;
                
            }
            
            return *this;
            
        }
        
        // Not equal operator:
        bool operator!=(const Iterator & other) const {
            
            return current != other.current;
            
        }
    };
    
    Iterator begin() const {
        
        return Iterator(first);
        
    }

    Iterator end() const {
        
        return Iterator(nullptr);
        
    }
    
};
