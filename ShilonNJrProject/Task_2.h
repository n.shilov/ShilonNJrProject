//
//  task_2.h
//  ShilovNJrProject
//
//  Created by Шилов Николай Николаевич on 30.04.2024.
//

#ifndef task_2_h
#define task_2_h


#endif /* task_2_h */

#include "task_1.h"


//=========================================================================================
class Object {
    
private:
    
    static uint64_t count;  // this counter is initialized in the main.cpp file
    
public:
    
    // Initialization:
    Object() {
        
        ++count;
        
    }
    
    // Copy:
    Object(Object const & object) {
        
        ++count;
        
    }
    
    // Destructor:
    ~Object() {
        
        --count;
        
    }
    
    // Count getter:
    static uint64_t get_count() {
        
        return count;
        
    }
    
    // Message:
    virtual std::string to_string() const = 0;
    
};
 

//=========================================================================================
class Task: virtual public Object {
    
private:
    
    // Completed flag:
    bool completed;
 
public:
    
    std::string to_string() const {
        
        std::string message = "This is a general Task object.";
        
        if (is_completed()) {
            
            message += " It has been completed.";
            
        }
        
        else {
            
            message += " It has not been completed yet.";
            
        }
        
        return message;
        
    }
    
    virtual bool may_have_result() const = 0;
    
    bool is_completed() const {
        
        return completed;
        
    }
    
    void complete_the_task() {
        
        completed = true;
        
    }
    
};


//=========================================================================================
class Named: virtual public Object {
    
private:
    
    std::string name;
 
public:
    
    Named(std::string const & name) {
        
        this -> name = name;
        
    }
    
    std::string get_name() const {
        
        return name;
        
    }

    std::string to_string() const {
        
        return std::string("This is a general named object. Its name is '" + get_name() + "'.");
        
    }
    
};


//=========================================================================================
class TaskWithResult: public Task {
    
private:
    
    float result;
    
public:
    
    std::string to_string() const {
        
        std::string message = std::string("This is a Task with result.");
        
        if (is_completed()) {
            
            message += " This Task has been completed. The result reads " + std::to_string(get_result()) + ".";
            
        }
        
        else {
            
            message += " This Task has not been completed yet.";
            
        }
        
        return message;
        
    }
    
    bool may_have_result() const {
        
        return true;
        
    }
    
    float get_result() const{
        
        return result;
        
    }
    
    void set_result(float const & result){
        
        this -> result = result;
        
    }
};


//=========================================================================================
class TaskWithoutResult: public Task {
    
public:
    
    std::string to_string() const {
        
        std::string message = std::string("This is a Task without result.");
        
        if (is_completed()) {
            
            message += " This Task has been completed.";
            
        }
        
        else {
            
            message += " This Task has not been completed yet.";
            
        }
        
        return message;
        
    }
    
    bool may_have_result() const {
        
        return false;
        
    }
    
};

//=========================================================================================
class BinaryArithmeticTask: public Named, public TaskWithResult {
 
public:
    
    BinaryArithmeticTask(std::string name): Named(name), TaskWithResult() {}
    
    std::string to_string() const {
        
        std::string message = std::string("This is a named binary arithmetic Task. Its name is '" + get_name() + "'.");
        
        if (is_completed()) {
            
            message += " This Task has been completed. The result reads " + std::to_string(get_result()) + ".";
            
        }
        
        else {
            
            message += " This Task has not been completed yet.";
            
        }
        
        return message;
        
    }
    
    void add(float const & a, float const & b) {
        
        if (is_completed()) {
            
            std::cout << "This Task is already completed" << std::endl;
            
            return;
            
        }
        
        set_result(a + b);
        
        complete_the_task();
        
    }
    
    void subtract(float const & a, float const & b) {
        
        if (is_completed()) {
            
            std::cout << "This Task is already completed" << std::endl;
            
            return;
            
        }
        
        set_result(a - b);
        
        complete_the_task();
        
    }
    
    void multiply(float const & a, float const & b) {
        
        if (is_completed()) {
            
            std::cout << "This Task is already completed" << std::endl;
            
            return;
            
        }
        
        set_result(a * b);
        
        complete_the_task();
        
    }
    
    void divide(float const & a, float const & b) {
        
        if (is_completed()) {
            
            std::cout << "This Task is already completed" << std::endl;
            
            return;
            
        }
        
        set_result(a / b);
        
        complete_the_task();
        
    }
    
};


//=========================================================================================
class AppendToContainerTask: public TaskWithoutResult {
    
private:
    
public:
    
    std::string to_string() const {
        
        std::string message = "This is a Task that appends another Task to a Container.";
        
        if (is_completed()) {
            
            message += " It has been completed.";
            
        }
        
        else {
            
            message += " It has not been completed yet.";
            
        }
        
        return message;
        
    }
    
    void append(Container<Task*> & container, Task * task) {
        
        if (is_completed()) {
            
            std::cout << "This Task is already completed" << std::endl;
            
            return;
            
        }
        
        complete_the_task();
    
        container.append(task);
        
    }
};


//=========================================================================================
class CountObjectsInContainerTask: public TaskWithResult {
    
public:
    
    std::string to_string() const {
        
        std::string message = "This is a Task that counts Objects stored in a Container.";
        
        if (is_completed()) {
            
            message += " This Task has been completed. The result reads " + std::to_string(get_result()) + ".";
            
        }
        
        else {
            
            message += " It has not been completed yet.";
            
        }
        
        return message;
        
    }
    
    void count_objects_in_container(Container<Object*> const & container) {
        
        if (is_completed()) {
            
            std::cout << "This Task is already completed" << std::endl;
            
            return;
            
        }
        
        set_result(container.get_size());
        
        complete_the_task();
        
    }
    
};


//=========================================================================================
class CountTasksWithResultTask: public TaskWithResult {
    
public:
    
    
    std::string to_string() const {
        
        std::string message = "This is a Task that counts Tasks with result in a Container.";
        
        if (is_completed()) {
            
            message += " This Task has been completed. The result reads " + std::to_string(get_result()) + ".";
            
        }
        
        else {
            
            message += " It has not been completed yet.";
            
        }
        
        return message;
        
    }
    
    void count_tasks_with_result(Container<Task*> const & container) {
        
        if (is_completed()) {
            
            std::cout << "This Task is already completed" << std::endl;
            
            return;
            
        }
        
        uint64_t task_count = 0;
        
        for (auto it = container.begin(); it != container.end(); ++it) {
                
            if ((*it) -> may_have_result()) {
                
                task_count++;
                
            }
        }
        
        set_result(task_count);
        
        complete_the_task();
        
    }
    
};


//=========================================================================================
template<typename Type>
class ClearContainerTask: public TaskWithoutResult {
    
private:
    
public:
    
    
    std::string to_string() const {
        
        std::string message = "This is a Task that clears a Container.";
        
        if (is_completed()) {
            
            message += " It has been completed.";
            
        }
        
        else {
            
            message += " It has not been completed yet.";
            
        }
        
        return message;
        
    }
    
    void clear_container(Container<Type> & container) const {
        
        if (is_completed()) {
            
            std::cout << "This Task is already completed" << std::endl;
            
            return;
            
        }
        
        container.clear();
        
    }
    
};


//=========================================================================================
class CountObjectsInProgramTask: public TaskWithResult {
    
public:
    
    std::string to_string() const {
        
        std::string message = "This is a Task that counts the number of Objects in the program.";
        
        if (is_completed()) {
            
            message += " This Task has been completed. The result reads " + std::to_string(get_result()) + ".";
            
        }
        
        else {
            
            message += " It has not been completed yet.";
            
        }
        
        return message;
        
    }
    
    void count_objects() {
        
        if (is_completed()) {
            
            std::cout << "This Task is already completed" << std::endl;
            
            return;
            
        }
        
        set_result(get_count());
        
        complete_the_task();
        
    }
    
};
