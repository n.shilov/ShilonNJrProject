//
//  main.cpp
//  ShilonNJrProject
//
//  Created by Шилов Николай Николаевич on 30.04.2024.
//

#include <iostream>
#include "task_2.h"

// Static counter initialization:
uint64_t Object::count = 0;

int main() {

    Container<Task*> task_container;  // If I use Task instead of Task*, all the Task children are casted to simple Tasks...
    Container<std::string> str_container;
    
    BinaryArithmeticTask * task_1 = new BinaryArithmeticTask("arithmetic 1");  // Object constructor is called once now!
    BinaryArithmeticTask * task_2 = new BinaryArithmeticTask(*task_1);  // Copying constructor!
    AppendToContainerTask * task_3 = new AppendToContainerTask;
    AppendToContainerTask * task_4 = new AppendToContainerTask;
    CountObjectsInContainerTask * task_5 = new CountObjectsInContainerTask;
    CountObjectsInContainerTask * task_6 = new CountObjectsInContainerTask;
    CountTasksWithResultTask * task_7 = new CountTasksWithResultTask;
    CountTasksWithResultTask * task_8 = new CountTasksWithResultTask;
    ClearContainerTask<Task*> * task_9 = new ClearContainerTask<Task*>;
    ClearContainerTask<Task*> * task_10 = new ClearContainerTask<Task*>;
    CountObjectsInProgramTask * task_11 = new CountObjectsInProgramTask;
    CountObjectsInProgramTask * task_12 = new CountObjectsInProgramTask;
    
    std::cout << "\nAll objects are created\n" << std::endl;
    
    std::cout << "Objects' count: " << Object::get_count() << std::endl;  // 12 - correct result!
    
    task_container.append(task_1);
    task_container.append(task_2);
    task_container.append(task_3);
    task_container.append(task_4);
    task_container.append(task_5);
    task_container.append(task_6);
    task_container.append(task_7);
    task_container.append(task_8);
    task_container.append(task_9);
    task_container.append(task_10);
    task_container.append(task_11);
    task_container.append(task_12);
    
    std::cout << "Task container is filled\n" << std::endl;
    
    std::cout << "Objects' count: " << Object::get_count() << std::endl;  // 12
    std::cout << "Task container size: " << task_container.get_size() << std::endl;  // 12
    std::cout << "String container size: " << str_container.get_size() << std::endl;  // 0
    
    // Completing the tasks:
    for (auto it = task_container.begin(); it != task_container.end();) {
        
        (*it) -> complete_the_task();
        str_container.append((*it) -> to_string());  // the results of the tasks will be all zero since we complete them forcibly
        
        ++it;
        task_container.pop_first();  // I need to increment explicitly and THEN pop the first element (delete the pointer to it),
        // otherwise the iterator can't increment since the pointer is already gone
        
        
    }
    
    std::cout << "\nTask container is exhausted and string container is filled\n" << std::endl;
    
    std::cout << "Objects' count: " << Object::get_count() << std::endl;  // Only pointers were deleted above, all the objects are still present
    std::cout << "Task container size: " << task_container.get_size() << std::endl;  // 0
    std::cout << "String container size: " << str_container.get_size() << std::endl; // 12
    
    // Printing the results:
    for (auto it = str_container.begin(); it != str_container.end(); ++it) {
        
        std::cout << (*it) << std::endl;
        
    }
    
    // Clear both containers:
    task_container.clear();
    str_container.clear();
    
    std::cout << "\nBoth containers are cleared\n" << std::endl;
    
    std::cout << "Objects' count: " << Object::get_count() << std::endl;  // 14
    std::cout << "Task container size: " << task_container.get_size() << std::endl;  // 0
    std::cout << "String container size: " << str_container.get_size() << std::endl; // 0
    
    // Delete the objects manually:
    delete task_1;
    delete task_2;
    delete task_3;
    delete task_4;
    delete task_5;
    delete task_6;
    delete task_7;
    delete task_8;
    delete task_9;
    delete task_10;
    delete task_11;
    delete task_12;
    
    std::cout << "\nAll objects are deleted\n" << std::endl;
    
    std::cout << "Objects' count: " << Object::get_count() << std::endl;  // 0
    
    return 0;

}
